package com.example.calculatorviapo.service;

import com.example.calculatorviapo.model.Matrix;
import com.example.calculatorviapo.utils.MatrixUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Service;

@Service
public class OperationService {

    private Logger logger = LoggerFactory.getLogger(OperationService.class);

    public Double addition(Double firstOperand, Double secondOperand) {
        logger.info("addition:" + firstOperand + "\t" + secondOperand);
        return firstOperand + secondOperand;
    }

    public Double subtraction(Double firstOperand, Double secondOperand) {
        logger.info("subtraction:" + firstOperand + "\t" + secondOperand);
        return firstOperand - secondOperand;
    }

    public Double multiplication(Double firstOperand, Double secondOperand) {
        logger.info("multiplication:" + firstOperand + "\t" + secondOperand);
        return firstOperand * secondOperand;
    }

    public Double division(Double firstOperand, Double secondOperand) {
        logger.info("division:" + firstOperand + "\t" + secondOperand);
        if (secondOperand == 0) {
            logger.warn("Dividing by zero.");
            return null;
        }
        return firstOperand / secondOperand;
    }

    public String testMatrixMultiplication() {
        Matrix a;
        Matrix b;

        int overallValue = getRandomNumber();
        a = new Matrix(getRandomNumber(), overallValue);
        b = new Matrix(overallValue, getRandomNumber());
        a.fillIn();
        b.fillIn();
        logger.info("input matrix A:" + a.toString());
        logger.info("input matrix B:" + b.toString());
        return MatrixUtils.multiplication(a, b).toString();
    }


    private static int getRandomNumber() {
        //We leave the probability that there will be a wrong matrix size.
        return (int) (Math.random() * 100 - 3);
    }
}