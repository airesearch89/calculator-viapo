package com.example.calculatorviapo;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class CalculatorViapoApplication {

    public static void main(String[] args) {
        SpringApplication.run(CalculatorViapoApplication.class, args);
    }

}
