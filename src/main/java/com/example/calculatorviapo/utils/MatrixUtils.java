package com.example.calculatorviapo.utils;

import com.example.calculatorviapo.model.Matrix;

public final class MatrixUtils {

    public static Matrix multiplication(Matrix a, Matrix b) {
        Matrix res = new Matrix(a.getSizeX(), b.getSizeY());


        for (int i = 0; i < res.getSizeX(); i++) {
            for (int j = 0; j < res.getSizeY(); j++) {
                for (int k = 0; k < b.getSizeX(); k++) {
                    res.setValue(i, j, a.getValue(i, k) * b.getValue(k, j));
                }
            }
        }

        return res;
    }
}
