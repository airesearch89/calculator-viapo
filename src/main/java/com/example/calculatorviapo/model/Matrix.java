package com.example.calculatorviapo.model;

public class Matrix {
    private final int sizeX;
    private final int sizeY;
    private final double[][] vector;

    public Matrix(int sizeX, int sizeY) {
        vector = new double[sizeX][sizeY];
        this.sizeX = sizeX;
        this.sizeY = sizeY;
    }

    public void fillIn() {
        for (int i = 0; i < sizeX; i++) {
            for (int j = 0; j < sizeY; j++) {
                vector[i][j] = getRandomNumber();
            }
        }
    }

    private static int getRandomNumber() {
        return (int) (Math.random() * 8000 - 4000);
    }

    public int getSizeX() {
        return sizeX;
    }

    public int getSizeY() {
        return sizeY;
    }

    public double getValue(int x, int y) {
        return vector[x][y];
    }

    public void setValue(int x, int y, double value) {
        vector[x][y] = value;
    }

    @Override
    public String toString() {
        StringBuilder stringBuilder = new StringBuilder();
        stringBuilder.append("Matrix{" + "sizeX=")
                .append(sizeX)
                .append(", sizeY=")
                .append(sizeY)
                .append(", vector=");

        for (int x = 0; x < sizeX; x++) {
            for (int y = 0; y < sizeY; y++) {
                stringBuilder.append("\t")
                        .append(getValue(x, y))
                        .append("\t");
            }
        }

        stringBuilder.append("...}");
        return stringBuilder.toString();
    }
}
