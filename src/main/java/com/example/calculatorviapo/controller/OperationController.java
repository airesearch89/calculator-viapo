package com.example.calculatorviapo.controller;

import com.example.calculatorviapo.service.OperationService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

@RestController
public class OperationController {

    @Autowired
    private OperationService operationService;

    @GetMapping("addition")
    public Double addition(@RequestParam(name = "firstOperand") Double firstOperand,
                           @RequestParam(name = "secondOperand") Double secondOperand) {
        return operationService.addition(firstOperand, secondOperand);
    }

    @GetMapping("subtraction")
    public Double subtraction(@RequestParam(name = "firstOperand") Double firstOperand,
                              @RequestParam(name = "secondOperand") Double secondOperand) {
        return operationService.subtraction(firstOperand, secondOperand);
    }

    @GetMapping("multiplication")
    public Double multiplication(@RequestParam(name = "firstOperand") Double firstOperand,
                                 @RequestParam(name = "secondOperand") Double secondOperand) {
        return operationService.multiplication(firstOperand, secondOperand);
    }

    @GetMapping("division")
    public Double division(@RequestParam(name = "firstOperand") Double firstOperand,
                           @RequestParam(name = "secondOperand") Double secondOperand) {
        return operationService.division(firstOperand, secondOperand);
    }

    @GetMapping("test-multiplication")
    public String testMultiplication()   {
        return operationService.testMatrixMultiplication();
    }
}
